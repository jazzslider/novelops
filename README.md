# NovelOps


## Features

### Markdown linting

NovelOps can check your project's Markdown files for syntax
issues as part of the build pipeline.  This is handled via
the [.pipelines/markdown-lint.gitlab-ci.yml](.pipelines/markdown-lint.gitlab-ci.yml)
include.

If you need to provide configuration options for the Markdown
linter, you can do so by including a `.mdlrc` file in the root
of your repository.  For more information on what you can do with
`.mdlrc`, please [visit the MDL documentation](https://github.com/markdownlint/markdownlint/blob/main/docs/configuration.md#creating-your-own-mdlrc-files).